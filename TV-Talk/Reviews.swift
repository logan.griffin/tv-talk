//
//  Reviews.swift
//  TV-Talk
//
//  Created by Logan Griffin on 14/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Reviews: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func setMovieImage(img: UIImage){
        let data = UIImagePNGRepresentation(img)
        self.image = data
    }
    
    func getMovieImage() -> UIImage {
        let img = UIImage(data: self.image!)
        return img!
    }
}
