//
//  ReviewCell.swift
//  TV-Talk
//
//  Created by Logan Griffin on 14/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieRating: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        movieImage.layer.cornerRadius = 7.0
        movieImage.clipsToBounds = true
    }

    func configureCell(review: Reviews) {
        movieTitle.text = review.title
        movieRating.text = review.rating
        movieYear.text = review.year
        movieImage.image = review.getMovieImage()
        movieGenre.text = review.genre
    }

}
