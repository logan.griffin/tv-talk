//
//  DetailsVC.swift
//  TV-Talk
//
//  Created by Logan Griffin on 15/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

    @IBOutlet weak var detailsImage: UIImageView!
    @IBOutlet weak var detailsTitle: UILabel!
    @IBOutlet weak var detailsRating: UILabel!
    @IBOutlet weak var detailsYear: UILabel!
    @IBOutlet weak var detailsGenre: UILabel!
    @IBOutlet weak var detailsDescription: UILabel!
    
    var str: Reviews?
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        configureView(str!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsImage.layer.cornerRadius = 5.0
        detailsImage.clipsToBounds = true
    }
    
    func configureView(review: Reviews){
        detailsImage.image = review.getMovieImage()
        detailsTitle.text = review.title
        detailsRating.text = review.rating
        detailsYear.text = review.year
        detailsGenre.text = review.genre
        detailsDescription.text = review.movieDescription

    }
    

}
