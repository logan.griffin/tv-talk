//
//  ViewController.swift
//  TV-Talk
//
//  Created by Logan Griffin on 13/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var reviews = [Reviews]()
    var reviewDetail: Reviews!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewDidAppear(animated: Bool) {
        fetchAndSetResults()
        tableView.reloadData()
    }
    
    func fetchAndSetResults() {
        let app = UIApplication.sharedApplication().delegate as? AppDelegate!
        let context = app!.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Reviews")
        
        do {
            let results = try context.executeFetchRequest(fetchRequest)
            self.reviews = results as! [Reviews]
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        reviewDetail = reviews[indexPath.row]
        performSegueWithIdentifier("gotoDetail", sender: reviewDetail)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "gotoDetail" {
            if let orangeViewController = segue.destinationViewController as? DetailsVC {
                orangeViewController.str = reviewDetail
            }
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
                
        if let cell = tableView.dequeueReusableCellWithIdentifier("ReviewCell") as? ReviewCell {
            let review = reviews[indexPath.row]
            cell.configureCell(review)
            return cell
            
        } else {
            return ReviewCell()
        }
    }
    
 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
}

