//
//  CreateReviewVC.swift
//  TV-Talk
//
//  Created by Logan Griffin on 14/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import UIKit
import CoreData

class CreateReviewVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var addMovieTitle: UITextField!
    @IBOutlet weak var addMovieRating: UITextField!
    @IBOutlet weak var addMovieGenre: UITextField!
    @IBOutlet weak var addMovieYear: UITextField!
    @IBOutlet weak var addMovieDescription: UITextField!
    @IBOutlet weak var addMovieImage: UIImageView!
    @IBOutlet weak var addMovieButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        addMovieImage.layer.cornerRadius = 7.0
        addMovieImage.clipsToBounds = true
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        addMovieImage.image = image
    }
    
    @IBAction func addImage (sender: AnyObject!) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func createReview(sender: AnyObject!){
        if let title = addMovieTitle.text where title != "" {
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            let entity = NSEntityDescription.entityForName("Reviews", inManagedObjectContext: context)!
            let review = Reviews(entity: entity, insertIntoManagedObjectContext: context)
            review.title = title
            review.rating = addMovieRating.text
            review.genre = addMovieGenre.text
            review.year = addMovieYear.text
            review.movieDescription = addMovieDescription.text
            review.setMovieImage(addMovieImage.image!)
            
            context.insertObject(review)
            
            do {
                try context.save()
            } catch {
                print("Could not save")
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    

}
