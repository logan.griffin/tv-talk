//
//  Reviews+CoreDataProperties.swift
//  TV-Talk
//
//  Created by Logan Griffin on 14/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Reviews {

    @NSManaged var image: NSData?
    @NSManaged var title: String?
    @NSManaged var rating: String?
    @NSManaged var genre: String?
    @NSManaged var movieDescription: String?
    @NSManaged var year: String?

}
